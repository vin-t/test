const VK = require('vk-io');
const _ = require('lodash');
const low = require('lowdb');

const vk = new VK;
const db = low('db.json');

console.log('IN DB');
console.log(db.value());

vk.setToken('');

update();

function update() {
  const resWall = {
    "walls": [
      {
        "id": -120485149
      }
    ],
    "items": []
  };

  resWall.walls.forEach(wall => {
    vk.api.wall.get({
      owner_id: wall.id,
      offset: 0,
      count: 5,
      extended: 1
    })
    .then(processData)
    .catch((error) => {
      console.error(error);
    });
  })

  function processData(data) {
    db.set('items', []).write();
    db.set('profiles', []).write();

    const items = data.items;
    const profiles = _.groupBy(data.profiles, 'id');

    items.forEach(item => {
      const userId = item.signer_id;
      const profile = profiles[userId][0];

      db.get('items').push({
        text: item.text,
        signer_id: item.signer_id,
        attachments: item.attachments || []
      }).write();

      db.get('profiles').push({
        id: userId,
        name: profile.first_name + ' ' + profile.last_name
      }).write();
    })
  }
}