const _ = require('lodash');

const data = { count: 26034,
  items:
   [ { id: 107880,
       from_id: -100017179,
       owner_id: -100017179,
       date: 1502305423,
       marked_as_ads: 0,
       post_type: 'post',
       text: 'Корпус Corsair SPEC-01 новый!\nКорпус в идеальном состоянии!\nПокупался в городе Ставрополь, в ДНС. \nБрал его месяц назад за 5399 руб\nПричина продажи-не подошёл для сборки!\nОтдам за 3500',
       signer_id: 181103724,
       is_pinned: 1,
       attachments: '',
       post_source: '',
       comments: '',
       likes: '',
       reposts: '',
       views: '' },
     { id: 107906,
       from_id: -100017179,
       owner_id: -100017179,
       date: 1502339438,
       marked_as_ads: 0,
       post_type: 'post',
       text: 'Продам провод 25кг Пв (плюсовой 7 метров, с предохранителем) Провод кг25 минус 7 метров и пару тюльпанов',
       signer_id: 249457242,
       attachments: '',
       post_source: '',
       comments: '',
       likes: '',
       reposts: '',
       views: '' },
     { id: 107905,
       from_id: -100017179,
       owner_id: -100017179,
       date: 1502339434,
       marked_as_ads: 0,
       post_type: 'post',
       text: 'Скутер .170 кубов .цена 15 т.р.или обмен на 50 кубов с документами .рассмотрю любые вырианты .',
       signer_id: 155175843,
       attachments: '',
       post_source: '',
       comments: '',
       likes: '',
       reposts: '',
       views: '' },
     { id: 107896,
       from_id: -100017179,
       owner_id: -100017179,
       date: 1502315271,
       marked_as_ads: 0,
       post_type: 'post',
       text: 'Продам, USB не работает, а так все хорошо, предлогайте цену в ЛС',
       signer_id: 141700975,
       attachments: '',
       post_source: '',
       comments: '',
       likes: '',
       reposts: '',
       views: '' },
     { id: 107893,
       from_id: -100017179,
       owner_id: -100017179,
       date: 1502312892,
       marked_as_ads: 0,
       post_type: 'post',
       text: 'Ремонт  грузовых и легковых авто,тюниг, ремонт ТНВД и форсунок диагностика форсунок установка турбин и т.д писать в личку!',
       signer_id: 264634780,
       attachments: '',
       post_source: '',
       comments: '',
       likes: '',
       reposts: '',
       views: '' } ]
 }

var items = data.items;
items = _.groupBy(items, i => i.id);   // группируем по id {'00001' : [...], '00002': [...]}

const allPostIds = Object.keys(items); // массив всех id-шников в ленте
const loadedIds = ['107880', '107893', '107896']; // массив уже загруженных id
const toLoad = [];  // массив id для загрузки

allPostIds.forEach(id => {
  // если id не найден, то значит есть новые посты
  if (!loadedIds.includes(id))
    toLoad.push(id)
})

console.log(`Нужно загрузить посты с ids ${toLoad}`);